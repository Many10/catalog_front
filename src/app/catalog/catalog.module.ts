import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShowListComponent } from './show-list/show-list.component';
import { ShowDetailComponent } from './show-detail/show-detail.component';
import { CastListComponent } from './cast-list/cast-list.component';
import { SeasonListComponent } from './season-list/season-list.component';
import { EpisodeSeasonListComponent } from './episode-season-list/episode-season-list.component';
import { EpisodeSearchComponent } from './episode-search/episode-search.component';
import { CatalogRoutingModule } from './catalog-routing.module';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    ShowListComponent,
    ShowDetailComponent,
    CastListComponent,
    SeasonListComponent,
    EpisodeSeasonListComponent,
    EpisodeSearchComponent
  ],
  imports: [
    CommonModule,
    CatalogRoutingModule,
    ReactiveFormsModule
  ]
})
export class CatalogModule { }
