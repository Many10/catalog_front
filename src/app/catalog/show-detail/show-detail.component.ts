import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ShowService } from 'src/app/core/services/show.service';
import { finalize } from 'rxjs/operators';
import { Show } from 'src/app/core/model/show.model';

@Component({
  selector: 'app-show-detail',
  templateUrl: './show-detail.component.html',
  styleUrls: ['./show-detail.component.css']
})
export class ShowDetailComponent implements OnInit {

  public idShow: string;
  public show: Show;
  public loading = false;
  public error: string;

  constructor(private route: ActivatedRoute,
              private showService: ShowService) { }

  ngOnInit() {
    this.route.params.subscribe( parameters => {
      const id = 'idShow';
      this.idShow = parameters[id];
      this.getShowById();
    });
  }

  private getShowById() {
    this.loading = true;
    this.showService.get(this.idShow).pipe(
      finalize(
        () => this.loading = false
      )
    ).subscribe(
      (response: Show) => {
        if (response != null) {
          this.show = response;
        }
      },
      error => {
        this.show = null;
        this.error = error.error;
      }
    );
  }

}
