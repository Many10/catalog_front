import { Component, OnInit } from '@angular/core';
import { Show } from 'src/app/core/model/show.model';
import { ShowService } from 'src/app/core/services/show.service';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-show-list',
  templateUrl: './show-list.component.html',
  styleUrls: ['./show-list.component.css']
})
export class ShowListComponent implements OnInit {

  public shows: Array<Show> = [];
  public loading = false;
  public error: string;
  public formGroup: FormGroup;

  constructor(private showService: ShowService) {
    this.formGroup = new FormGroup({
      name: new FormControl('', Validators.required),
      language: new FormControl('', Validators.required),
      genre: new FormControl('', Validators.required),
      channel: new FormControl('', Validators.required),
      scheduleTime: new FormControl('', Validators.required),
    });
  }

  ngOnInit() {
    this.getShowList();
  }

  private getShowList() {
    this.loading = true;
    this.showService.getAll().pipe(
      finalize(
        () => this.loading = false
      )
    ).subscribe(
      (response: Array<Show>) => {
        if (response != null) {
          this.shows = response;
        }
      },
      error => {
        this.shows = [];
        this.error = error.error;
      }
    );
  }

  private filter() {
    this.loading = true;
    const name = this.formGroup.get('name').value;
    const language = this.formGroup.get('language').value;
    const genre = this.formGroup.get('genre').value;
    const channel = this.formGroup.get('channel').value;
    const scheduleTime = this.formGroup.get('scheduleTime').value;
    console.log(channel);
    console.log(scheduleTime);
    this.showService.filter(name, language, genre, channel, scheduleTime).pipe(
      finalize(
        () => this.loading = false
      )
    ).subscribe(
      (response: Array<Show>) => {
        if (response != null) {
          this.shows = response;
        }
      },
      error => {
        this.shows = [];
        this.error = error.error;
      }
    );
  }

}
