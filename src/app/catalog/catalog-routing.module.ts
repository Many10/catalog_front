import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShowListComponent } from './show-list/show-list.component';
import { ShowDetailComponent } from './show-detail/show-detail.component';


const catalogRoutes: Routes = [
  {
      path: 'show',
      component: ShowListComponent
  },
  {
      path: 'show/detail/:idShow',
      component: ShowDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(catalogRoutes)],
  exports: [RouterModule]
})
export class CatalogRoutingModule { }
