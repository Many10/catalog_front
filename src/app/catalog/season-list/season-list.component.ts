import { Component, OnInit, Input } from '@angular/core';
import { Season } from 'src/app/core/model/season.model';
import { SeasonService } from 'src/app/core/services/season.service';
import { finalize } from 'rxjs/operators';
import { Cast } from 'src/app/core/model/cast.model';

@Component({
  selector: 'app-season-list',
  templateUrl: './season-list.component.html',
  styleUrls: ['./season-list.component.css']
})
export class SeasonListComponent implements OnInit {

  @Input()
  private idShow: string;
  private seasons: Array<Season>;
  private loading = false;
  private error: string;

  constructor(private seasonService: SeasonService) { }

  ngOnInit() {
    this.getSeasonsByShowId();
  }

  public getSeasonsByShowId() {
    this.loading = true;
    this.seasonService.getByShowId(this.idShow).pipe(
      finalize(
        () => this.loading = false
      )
    ).subscribe(
      (response: Array<Season>) => {
        if (response != null) {
          this.seasons = response;
        }
      },
      error => {
        this.seasons = [];
        this.error = error.error;
      }
    );
  }

}
