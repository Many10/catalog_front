import { Component, OnInit, Input } from '@angular/core';
import { Episode } from 'src/app/core/model/episode.model';
import { EpisodeService } from 'src/app/core/services/episode.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-episode-season-list',
  templateUrl: './episode-season-list.component.html',
  styleUrls: ['./episode-season-list.component.css']
})
export class EpisodeSeasonListComponent implements OnInit {

  @Input()
  private idSeason: string;
  private episodes: Array<Episode> = [];
  private loading = false;
  private error: string;

  constructor(private episodeService: EpisodeService) { }

  ngOnInit() {
    this.getCastByShowId();
  }

  private getCastByShowId() {
    this.loading = true;
    this.episodeService.getBySeasonId(this.idSeason).pipe(
      finalize(
        () => this.loading = false
      )
    ).subscribe(
      (response: Array<Episode>) => {
        if (response != null) {
          this.episodes = response;
        }
      },
      error => {
        this.episodes = [];
        this.error = error.error;
      }
    );
  }

}
