import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Episode } from 'src/app/core/model/episode.model';
import { EpisodeService } from 'src/app/core/services/episode.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-episode-search',
  templateUrl: './episode-search.component.html',
  styleUrls: ['./episode-search.component.css']
})
export class EpisodeSearchComponent implements OnInit {

  public formGroup: FormGroup;
  public episodesFilter: Array<Episode> = [];
  public loading = false;
  public error: string;
  @Input()
  public idShow: string;

  constructor(private episodeService: EpisodeService) {
    this.formGroup = new FormGroup({
      date: new FormControl('', Validators.required)
    });
  }

  ngOnInit() {
  }

  private filterEpisodesByDate() {
    this.loading = true;
    const date = this.formGroup.get('date').value;
    this.episodeService.getEpisodeByDate(this.idShow, date).pipe(
      finalize (
        () => this.loading = false
      )
    ).subscribe(
      (response: Array<Episode>) => {
        if (response != null) {
          this.episodesFilter = response;
        }
      },
      error => {
        this.episodesFilter = [];
        this.error = 'No se han encontrado episodios para la fecha seleccionada';
      }
    );
  }

}
