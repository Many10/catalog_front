import { Component, OnInit, Input } from '@angular/core';
import { Cast } from 'src/app/core/model/cast.model';
import { CastService } from 'src/app/core/services/cast.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-cast-list',
  templateUrl: './cast-list.component.html',
  styleUrls: ['./cast-list.component.css']
})
export class CastListComponent implements OnInit {

  @Input()
  private idShow: string;
  private cast: Array<Cast> = [];
  private loading = false;
  private error: string;

  constructor(private castService: CastService) { }

  ngOnInit() {
    this.getCastByShowId();
  }

  private getCastByShowId() {
    this.loading = true;
    this.castService.getByShowId(this.idShow).pipe(
      finalize(
        () => this.loading = false
      )
    ).subscribe(
      (response: Array<Cast>) => {
        if (response != null) {
          this.cast = response;
        }
      },
      error => {
        this.cast = [];
        this.error = error.error;
      }
    );
  }

}
