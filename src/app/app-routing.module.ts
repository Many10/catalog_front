import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './shared/dashboard/dashboard.component';

const catalogModule = () => import('./catalog/catalog.module').then(mod => mod.CatalogModule);
const userModule = () => import('./users/users.module').then(mod => mod.UsersModule);

const appRoutes: Routes = [
  {
      path: 'dashboard',
      component: DashboardComponent,
      data: { title: 'Inicio' },
      children: [
        {
          path: 'catalog',
          loadChildren: catalogModule
        },
        {
          path: 'admin',
          loadChildren: userModule
        }
      ]
  },
  {
    path: '',
    redirectTo: '/dashboard/catalog/show',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '/dashboard/catalog/show',
    pathMatch: 'full'
  }
];


@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
