export interface Season {
    id: number;
    name: string;
    language: string;
    genre: string;
    image: string;
    channel: string;
    scheduleTime: string;
    summary: string;
}
