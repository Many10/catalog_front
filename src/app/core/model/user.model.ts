import { Role } from './role.model';

export interface User {
    id: number;
    username: string;
    password: string;
    fullName: string;
    role: Role;
    createdAt: Date;
    createdBy: string;
    updatedAt: Date;
    updatedBy: string;
}
