export interface Cast {
    id: number;
    name: string;
    country: string;
    birthday: string;
    gender: string;
    image: string;
    character: string;
}
