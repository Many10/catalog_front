export interface Episode {
    id: number;
    name: string;
    order: number;
    releaseDate: string;
    duration: string;
    summary: string;
    image: string;
}
