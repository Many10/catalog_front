import { Injectable } from '@angular/core';
import { ServiceService } from './service.service';
import { Show } from '../model/show.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ShowService extends ServiceService<Show> {

  constructor(private http: HttpClient) {
      super();
      this.apiUrl = environment.api;
      this.httpClient = http;
      this.resource = 'show/';
  }

  public filter(name: string, language: string, genre: string, channel: string, scheduleTime: string) {
    const path = 'filter?keyname=' + name + '&language=' + language +
                 '&genre=' + genre + '&channel=' + channel + '&scheduleTime=' + scheduleTime;
    return this.executeGet(path).pipe(
      map((res: Array<Show>) => res)
    );
  }

}
