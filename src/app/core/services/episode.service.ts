import { Injectable } from '@angular/core';
import { ServiceService } from './service.service';
import { Episode } from '../model/episode.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EpisodeService extends ServiceService<Episode> {

  constructor(private http: HttpClient) {
      super();
      this.apiUrl = environment.api;
      this.httpClient = http;
      this.resource = 'episode/';
  }

  public getBySeasonId(seasonId: string) {
    const path = 'season/' + seasonId;
    return this.executeGet(path).pipe(
      map((res: Array<Episode>) => res)
    );
  }

  public getEpisodeByDate(showId: string, date: any) {
    const path = 'filter/' + showId + '/' + date;
    return this.executeGet(path).pipe(
      map((res: Array<Episode>) => res)
    );
  }

}
