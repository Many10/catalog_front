import { Injectable } from '@angular/core';
import { ServiceService } from './service.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Cast } from '../model/cast.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CastService extends ServiceService<Cast> {

  constructor(private http: HttpClient) {
      super();
      this.apiUrl = environment.api;
      this.httpClient = http;
      this.resource = 'cast/';
  }

  public getByShowId(showId: string) {
    const path = 'show/' + showId;
    return this.executeGet(path).pipe(
      map((res: Array<Cast>) => res)
    );
  }

}
