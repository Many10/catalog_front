import { Injectable } from '@angular/core';
import { ServiceService } from './service.service';
import { Season } from '../model/season.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SeasonService extends ServiceService<Season> {

  constructor(private http: HttpClient) {
      super();
      this.apiUrl = environment.api;
      this.httpClient = http;
      this.resource = 'season/';
  }

  public getByShowId(showId: string) {
    const path = 'show/' + showId;
    return this.executeGet(path).pipe(
      map((res: Array<Season>) => res)
    );
  }

}
