import { Injectable } from '@angular/core';
import { ServiceService } from './service.service';
import { Role } from '../model/role.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RoleService extends ServiceService<Role> {

  constructor(private http: HttpClient) {
      super();
      this.apiUrl = environment.api;
      this.httpClient = http;
      this.resource = 'role/';
  }

}
