import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/core/services/user.service';
import { finalize } from 'rxjs/operators';
import { User } from 'src/app/core/model/user.model';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  public users: Array<User> = [];
  public userEdit: User;
  public error: string;
  public loading = false;
  public addUser = false;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.getUserList();
  }

  private getUserList() {
    this.loading = true;
    this.userService.getAll().pipe(
      finalize(
        () => this.loading = false
      )
    ).subscribe(
      (users: Array<User>) => {
        if (users != null) {
          this.users = users;
        }
      },
      error => {
        this.users = [];
        this.error = error.error;
      }
    );
  }

  private deleteUser(idUser: string) {
    this.loading = true;
    this.userService.delete(idUser).pipe(
      finalize(
        () => this.loading = false
      )
    ).subscribe(
      response => {
          this.getUserList();
      },
      error => {
        this.users = [];
        this.error = error.error;
      }
    );
  }

  private showAddUser(user: any) {
    this.addUser = !this.addUser;
    this.userEdit = user;
  }

  private onReloadEvent(reload: boolean) {
    if (reload) {
      this.getUserList();
    }
  }

}
