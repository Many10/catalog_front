import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersAddComponent } from './users-add/users-add.component';
import { UsersListComponent } from './users-list/users-list.component';
import { UserRoutingModule } from './user-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [UsersAddComponent, UsersListComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    ReactiveFormsModule
  ]
})
export class UsersModule { }
