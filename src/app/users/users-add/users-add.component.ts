import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { UserService } from 'src/app/core/services/user.service';
import { User } from 'src/app/core/model/user.model';
import { Role } from 'src/app/core/model/role.model';
import { RoleService } from 'src/app/core/services/role.service';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-users-add',
  templateUrl: './users-add.component.html',
  styleUrls: ['./users-add.component.css']
})
export class UsersAddComponent implements OnInit {

  public formGroup: FormGroup;
  public loading = false;
  public error: string;
  public roles: Array<Role> = [];
  @Output()
  public reload = new EventEmitter<boolean>();
  @Input()
  public userEdit: User;

  constructor(private userService: UserService,
              private roleService: RoleService) {
    this.formGroup = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      fullName: new FormControl('', Validators.required),
      role: new FormControl('', Validators.required),
    });
   }

  ngOnInit() {
    this.getRoles();
    this.fillForm();
  }

  private getRoles() {
    this.loading = true;
    this.roleService.getAll().pipe(
      finalize(
        () => this.loading = false
      )
    ).subscribe(
      (response: Array<Role>) => {
        if (response != null) {
          this.roles = response;
        }
      },
      error => {
        this.roles = [];
        this.error = error.error;
      }
    );
  }

  private onSubmit() {
    if (this.userEdit != null) {
      this.update();
    } else {
      this.save();
    }
  }

  private save() {
    const role = {
      id: this.formGroup.get('role').value
    } as Role;
    const newUser = {
      username: this.formGroup.get('username').value,
      password: this.formGroup.get('password').value,
      fullName: this.formGroup.get('fullName').value,
      role: role,
      createdAt: new Date(),
      createdBy: 'sessionUser', // Get username from session
      updatedAt: new Date(),
      updatedBy: 'sessionUser', // Get username from session
    } as User;
    this.userService.post(newUser).pipe(
      finalize(
        () => this.loading = false
      )
    ).subscribe(
      response => {
        this.reload.next(true);
        this.formGroup.reset();
      },
      error => {
        this.error = error.error;
      }
    );
  }

  private update() {
    const role = {
      id: this.formGroup.get('role').value
    } as Role;
    const newUser = {
      username: this.formGroup.get('username').value,
      password: this.formGroup.get('password').value,
      fullName: this.formGroup.get('fullName').value,
      role: role,
      createdAt: new Date(),
      createdBy: 'sessionUser', // Get username from session
      updatedAt: new Date(),
      updatedBy: 'sessionUser', // Get username from session
    } as User;
    this.userService.update(this.userEdit.id + '', newUser).pipe(
      finalize(
        () => this.loading = false
      )
    ).subscribe(
      response => {
        this.reload.next(true);
      },
      error => {
        this.error = error.error;
      }
    );
  }

  private fillForm() {
    if (this.userEdit != null) {
      this.formGroup = new FormGroup({
        username: new FormControl(this.userEdit.username, Validators.required),
        password: new FormControl(this.userEdit.password, Validators.required),
        fullName: new FormControl(this.userEdit.fullName, Validators.required),
        role: new FormControl(this.userEdit.role.id, Validators.required),
      });
    }
  }
}
